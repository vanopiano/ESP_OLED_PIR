#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <ArduinoJson.h>
#include <Ticker.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <ESP8266mDNS.h>

#define OLED_CLK   D5
#define OLED_MOSI  D7
#define OLED_CS    D8
#define OLED_DC    D2
#define OLED_RESET D4

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

const char* workssid = "guest";
const char* workpassword = "wlhub1337";

Adafruit_SSD1306 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);
ESP8266WebServer server(80);
ESP8266WiFiMulti WiFiMulti;
MDNSResponder mdns;
Ticker pirChecker;

String valueFromForm = "", pirStatus = "", pir2Status = "";
#define sensorPin  D1
#define TIME_TO_LIGHT  5 // in seconds
#define DNS "room422" // in seconds

boolean turned = false;
unsigned long startTime = 0;

void setupWIFI() {
  // WiFi.begin(workssid, workpassword);
  //
  // // Wait for connection
  // while (WiFi.status() != WL_CONNECTED) {
  //   delay(500);
  // }
  //
  WiFiMulti.addAP(workssid, workpassword);
  // WiFiMulti.addAP(homessid, homepassword);
  // WiFiMulti.addAP(mobilessid, mobilepassword);

  // WiFi.mode(WIFI_STA);
  mdns.begin(DNS, WiFi.localIP());

  while(WiFiMulti.run() != WL_CONNECTED) {
    delay(100);
  }
}

void setupDisplay() {
  display.begin(SSD1306_SWITCHCAPVCC);
  display.setTextSize(1);

  display.setTextColor(WHITE);

  // Inbert BG only for text (rectangle undre the text wiill be white, text will be black)
  // display.setTextColor(BLACK, WHITE);

  // // Invert all display
  // display.invertDisplay(true);
  // // White will be black on the white with this settings
  // display.setTextColor(WHITE);

  display.clearDisplay();
  // display.display();
  // delay(3000);
}

void simplePrint(int y, String value) {
  display.setCursor(4,y);
  display.println(value);
  display.display();
}

void checkPirStatus()  {
  int state = digitalRead(sensorPin);
  if(state == HIGH) {
    // if (!turned) {
    //   startTime = millis();
    // }
    pir2Status = "State is HIGH";
    startTime = millis()/1000;
    on();
    regularPrint();

  } else {
    pir2Status = "State is LOW";
    if (turned && (millis()/1000 - startTime) > TIME_TO_LIGHT) {
      off();
    }
    regularPrint();
  }
}

void regularPrint() {
  display.clearDisplay();
  XYPrint(12, 4, "IP: " + WiFi.localIP().toString());
  // display.setCursor(0,16);
  // display.println("http://" + DNS + ".local/");
  // display.setCursor(1,27);
  display.setCursor(1,23);
  display.println(valueFromForm);
  display.setCursor(1,40);
  display.println(pirStatus);
  display.setCursor(1,55);
  display.println(pir2Status);
  display.display();
}

void XYPrint(int x, int y, String value) {
  display.setCursor(x,y);
  display.println(value);
  display.display();
}

void handleRoot() {
	char temp[1900];
	int sec = millis() / 1000;
	int min = sec / 60;
	int hr = min / 60;
        snprintf ( temp, 1900,
    "<html>"
    "<head>"
    "<!--<meta http-equiv='refresh' content='10'/> -->"
    "<link rel='stylesheet' href='https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.min.css' />"
    "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>"
    "<script src='https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/js/bootstrap.min.js'></script>"
    "<title>220V Control</title>"
    "<style>"
    "  body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }"
    "</style>"
    "</head>"
    "<body>"
    "  <h1>Text on Display Control</h1>"
    "  <p>Uptime: %02d:%02d:%02d</p>"
    "  <p>PIR status: %s"
    "</p>"
    "  <form id='textform' action='/text'>"
    "    <input type='text' name='text'>"
    "    <input type='submit' value='Send text'>"
    "  </form>"
    //"  <button class='btn btn-danger' id='turnoff'>OFF</button>"
    //"  <button class='btn btn-success' id='turnon'>ON</button>"
    "  <script>"
    //"      $('#turnoff').click(function() {"
    //"        $.post('/controlAjax?a=0');"
    //"        $('#turnoff').prop('disabled', 'disabled');"
    //"        $('#turnon').prop('disabled', false);"
    //"      });"
    //"      $('#turnon').click(function() {"
    //"        $.post('/controlAjax?a=1');"
    //"        $('#turnoff').prop('disabled', false);"
    //"        $('#turnon').prop('disabled', 'disabled');"
    //"      });"
    "      $(document).ready(function() {"
    "        $('#textform').submit(function(event) {"
    "          var formData = { 'text': $('input[name=text]').val() };"
    "          $.ajax({"
    "            type        : 'POST',"
    "            url         : '/text',"
    "            data        : formData"
    "          }).done(function(data) {"
    "            console.log(data); "
    "          });"
    "          event.preventDefault();"
    "        });"
    "      });"
    "  </script>"
    "</body>"
    "</html>",

		hr, min % 60, sec % 60, pirStatus.c_str()
	);
	server.send ( 200, "text/html", temp );
}

void handleNotFound(){
  String message = "Page Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void handleAPIWeather() {
  String weather = "24 degrees\n";
  // weather += "HEADERS:\n";
  for (int i = 0; i < server.headers(); i++) {
    weather += server.headerName(i) + ": " + server.header(i) + "\n";
  }

  if (server.hasHeader("Content-Type")) {
    server.send(200, "application/json", "{\"weather\": "+weather + "}");
  } else {
    server.send(200, "text/plain", weather);
  }
}

void handleText() {
  // weather += "HEADERS:\n";
  for (uint8_t i=0; i < server.args(); i++){
    if (server.argName(i) == "text") {
      valueFromForm = server.arg(i);
      regularPrint();
    }
  }
  server.send(200, "text/plain", "Ok");
}

void setup(void){
  setupDisplay();
  setupWIFI();
  // Serial.begin(9600);

  pinMode(sensorPin, INPUT);

  XYPrint(12, 4, "IP: " + WiFi.localIP().toString());

  pirChecker.attach(0.8, checkPirStatus); // each 0.8 seconds
  server.on("/", handleRoot);
  server.on("/text", handleText);
  // server.on("/controlAjax", controlAjaxLight);
  // server.on("/api", handleApi);
  // server.on("/status", handleStatus);

  server.onNotFound(handleNotFound);
  server.begin();

  startTime = millis();
}

void on() {
  pirStatus = "Motion detected";
  // regularPrint();
  turned = true;
}

void off() {
  pirStatus = "No motion...";
  // regularPrint();
  turned = false;
}


void loop(void){
  server.handleClient();
}
